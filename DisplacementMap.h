#pragma once
#ifndef _DISPLACEMENT_H_
#define _DISPLACEMENT_H_

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <iostream>
#include <vector>

class DisplaceMapVert
{
	public:
		Eigen::Vector3f disp;
		double dist;
		int vertNum;
};

std::vector<DisplaceMapVert> mergeSort(std::vector<DisplaceMapVert> m);

#endif