#ifndef __Texture__
#define __Texture__

#ifdef __APPLE__
#include <GLUT/glut.h>
#endif
#ifdef __unix__
#include <GL/glut.h>
#endif
#ifdef _WIN32
#define GLFW_INCLUDE_GLCOREARB
#include <GL/glew.h>
#include <cstdlib>
#include <glut.h>
#endif
#include <string>
#include <Eigen/Dense>
#include <SDL/SDL.h>

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    const uint32_t rmask = 0xff000000;
    const uint32_t gmask = 0x00ff0000;
    const uint32_t bmask = 0x0000ff00;
    const uint32_t amask = 0x000000ff;
    const uint8_t  rshift = 24;
    const uint8_t  gshift = 16;
    const uint8_t  bshift = 8; 
#else
    const uint32_t rmask = 0x000000ff;
    const uint32_t gmask = 0x0000ff00;
    const uint32_t bmask = 0x00ff0000;
    const uint32_t amask = 0xff000000;
    const uint8_t  rshift = 0;
    const uint8_t  gshift = 8;
    const uint8_t  bshift = 16; 
#endif	

class Texture
{
public:
	Texture();
	virtual ~Texture();
	void setFilename(const std::string &f) { filename = f; }
	void init();
	void bind(GLint handle, GLint unit);
	void unbind();
	int width;
	int height;
	Eigen::Vector3f getPixel(int x, int y) const;
	
private:
	std::string filename;
	GLuint tid;
	GLint unit;
    SDL_Surface *rv;	
};

Uint32 get_pixel32(SDL_Surface *surface, int x, int y);


#endif
