#ifdef __APPLE__
#include <GLUT/glut.h>
#endif
#ifdef __unix__
#include <GL/glut.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <memory>
#include "GLSL.h"
#include "Program.h"
#include "Camera.h"
#include "MatrixStack.h"
#include "ShapeObj.h"
#include "HUD.h"

#include "BlendShape.h"

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>

using namespace std;

bool keyToggles[256] = {false};
bool line_mode = false;

int width = 1;
int height = 1;

Program prog;
Camera camera;

GLuint posBufID, norBufID, idxBufID;

BlendShape myface;

float blendWeights[6] = {0, 0, 0, 0, 0, 0};
int pickedEmote = 0;
int spotGroup = 1;

void drawStuff(int h_pos, int h_nor);

int getPercent(float val) {
	int ret = val*100;

	return ret;
}

void loadScene()
{
	myface.loadMesh("models/neutral.obj");
	myface.addBlend("models/happy.obj");	//happy
	myface.addBlend("models/anger.obj");	//anger
	myface.addBlend("models/alert.obj");	//alert
	myface.addBlend("models/concern.obj");	//concern
	myface.addBlend("models/disgust.obj");	//disgust
	myface.addBlend("models/sadness.obj");	//sadness, it greatly effects the cheeks
	keyToggles['c'] = true;
	
	prog.setShaderNames("simple_vert.glsl", "simple_frag.glsl");
}

void initGL()
{
	//////////////////////////////////////////////////////
	// Initialize GL for the whole scene
	//////////////////////////////////////////////////////
	
	// Set background color
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	// Enable z-buffer test
	glEnable(GL_DEPTH_TEST);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//////////////////////////////////////////////////////
	// Intialize the shaders
	//////////////////////////////////////////////////////
	
	prog.init();
	prog.addUniform("P");
	prog.addUniform("MV");
	prog.addAttribute("vertPos");
	prog.addAttribute("vertNor");

	prog.addAttribute("blendPose1");
	prog.addAttribute("blendNorm1");

	prog.addAttribute("blendPose2");
	prog.addAttribute("blendNorm2");

	prog.addAttribute("blendPose3");
	prog.addAttribute("blendNorm3");

	prog.addAttribute("blendPose4");
	prog.addAttribute("blendNorm4");

	prog.addAttribute("blendPose5");
	prog.addAttribute("blendNorm5");

	prog.addAttribute("blendPose6");
	prog.addAttribute("blendNorm6");

	prog.addAttribute("group");
	
	prog.addUniform("uMat.aColor");
	prog.addUniform("uMat.dColor");
	prog.addUniform("uMat.sColor");
	prog.addUniform("uMat.shine");
	prog.addUniform("isDot");
	prog.addUniform("spotGroup");

	prog.addUniform("weights");

	//////////////////////////////////////////////////////
	// Intialize the shapes
	//////////////////////////////////////////////////////
	myface.init();

	//////////////////////////////////////////////////////
	// Bind Buffers manually like scrubs 
	//////////////////////////////////////////////////////

    // Unbind the arrays
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//////////////////////////////////////////////////////
	// Final check for errors
	//////////////////////////////////////////////////////
	GLSL::checkVersion();
}

void reshapeGL(int w, int h)
{
	// Set view size
	width = w;
	height = h;
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	camera.setAspect((float)width/(float)height);
	HUD::setWidthHeight(w, h);
}

void drawGL()
{	
	// Clear buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if(keyToggles['c']) {
		glEnable(GL_CULL_FACE);
	} else {
		glDisable(GL_CULL_FACE);
	}
	
	//////////////////////////////////////////////////////
	// Create matrix stacks
	//////////////////////////////////////////////////////
	
	MatrixStack P, MV;

	// Apply camera transforms
	P.pushMatrix();
	camera.applyProjectionMatrix(&P);
	MV.pushMatrix();
	camera.applyViewMatrix(&MV);
	
	//////////////////////////////////////////////////////
	// Draw origin frame using old-style OpenGL
	// (before binding the program)
	//////////////////////////////////////////////////////
	
	// Setup the projection matrix
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadMatrixf(P.topMatrix().data());
	
	// Setup the modelview matrix
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadMatrixf(MV.topMatrix().data());
	
	//drawVolume(false);
	// Pop modelview matrix
	glPopMatrix();
	
	// Pop projection matrix
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	
	// Draw string
	P.pushMatrix();
	P.ortho(0, width, 0, height, -1, 1);
	glPushMatrix();
	glLoadMatrixf(P.topMatrix().data());
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glRasterPos2f(5.0f, 5.0f);
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	P.popMatrix();
	
	//////////////////////////////////////////////////////
	// Now draw the shape using modern OpenGL
	//////////////////////////////////////////////////////
	
	// Bind the program
	prog.bind();
	
	// Send projection matrix
	glUniformMatrix4fv(prog.getUniform("P"), 1, GL_FALSE, P.topMatrix().data());
	glUniformMatrix4fv(prog.getUniform("MV"), 1, GL_FALSE, MV.topMatrix().data());
	
	//tell the shader to draw the rest with phong shading
	if (keyToggles['s']) {
		glUniform1f(prog.getUniform("isDot"), 1.0f);
	} else {
		glUniform1f(prog.getUniform("isDot"), 0.0f);
	}

	glUniform1f(prog.getUniform("spotGroup"), (float)spotGroup);

	glUniform1fv(prog.getUniform("weights"), 6, blendWeights);

	MV.pushMatrix();
		glUniform3f(prog.getUniform("uMat.aColor"), 0.2, 0.2, 0.2);
		glUniform3f(prog.getUniform("uMat.dColor"), 0.8, 0.28, 0.3);
		glUniform3f(prog.getUniform("uMat.sColor"), 0.2, 0.1, 0.1);
		glUniform1f(prog.getUniform("uMat.shine"), 200);
		myface.draw(&prog);
	MV.popMatrix();

	// Unbind the program
	prog.unbind();

	//////////////////////////////////////////////////////
	// Cleanup
	//////////////////////////////////////////////////////
	
	// Pop stacks
	MV.popMatrix();
	P.popMatrix();
	
	// Draw stats
	char str[256];
	sprintf(str, "Happy:   %d", getPercent(blendWeights[0]));
	
	if (pickedEmote == 0)
		glColor4f(1, 0, 0, 1);
	else 
		glColor4f(0,0,0,1);

	HUD::drawString(10, 60, str);

	sprintf(str, "Anger:   %d", getPercent(blendWeights[1]));
	
	if (pickedEmote == 1)
		glColor4f(.7, .3, 0, 1);
	else 
		glColor4f(0,0,0,1);
	
	HUD::drawString(10, 50, str);

	sprintf(str, "Alert:   %d", getPercent(blendWeights[2]));
	
	if (pickedEmote == 2)
		glColor4f(3, .7, 0, 1);
	else 
		glColor4f(0,0,0,1);
	
	HUD::drawString(10, 40, str);

	sprintf(str, "Concern: %d", getPercent(blendWeights[3]));
	
	if (pickedEmote == 3)
		glColor4f(0, 1, 0, 1);
	else 
		glColor4f(0,0,0,1);
	
	HUD::drawString(10, 30, str);

	sprintf(str, "Disgust: %d", getPercent(blendWeights[4]));
	
	if (pickedEmote == 4)
		glColor4f(0, .7, 3, 1);
	else 
		glColor4f(0,0,0,1);
	
	HUD::drawString(10, 20, str);

	sprintf(str, "Sad:     %d", getPercent(blendWeights[5]));
	
	if (pickedEmote == 5)
		glColor4f(0, .0, 1, 1);
	else 
		glColor4f(0,0,0,1);
	
	HUD::drawString(10, 10, str);

	// Swap buffer
	glutSwapBuffers();
}

void mouseGL(int button, int state, int x, int y)
{
	int modifier = glutGetModifiers();
	bool shift = modifier & GLUT_ACTIVE_SHIFT;
	bool ctrl  = modifier & GLUT_ACTIVE_CTRL;
	bool alt   = modifier & GLUT_ACTIVE_ALT;
	camera.mouseClicked(x, y, shift, ctrl, alt);
}

void mouseMotionGL(int x, int y)
{
	camera.mouseMoved(x, y);
}

void keyboardGL(unsigned char key, int x, int y)
{
	keyToggles[key] = !keyToggles[key];
	switch(key) {
		case 27:
			// ESCAPE
			exit(0);
			break;
		case 'l':
			if (!line_mode)
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			else
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			line_mode = !line_mode;
			break;
		case '1':
			pickedEmote = 0;
			break;
		case '2':
			pickedEmote = 1;
			break;
		case '3':
			pickedEmote = 2;
			break;
		case '4':
			pickedEmote = 3;
			break;
		case '5':
			pickedEmote = 4;
			break;
		case '6':
			pickedEmote = 5;
			break;
		case 'q':
			spotGroup = 1;
			break;
		case 'w':
			spotGroup = 2;
			break;
		case 'e':
			spotGroup = 3;
			break;
		case 'r':
			spotGroup = 4;
			break;
		case 't':
			spotGroup = 5;
			break;
		case 'y':
			spotGroup = 6;
			break;
		case 'h':
			blendWeights[pickedEmote] += .05;
			if (blendWeights[pickedEmote] > 1) {
				blendWeights[pickedEmote] = 1;
			}
			break;
		case 'g':
			blendWeights[pickedEmote] -= .05;
			if (blendWeights[pickedEmote] < 0) {
				blendWeights[pickedEmote] = 0;
			}
			break;
	}
}


void SpecialInput(int key, int x, int y)
{
	if (keyToggles[s]) {
		switch(key)
		{
			case GLUT_KEY_UP:
			myface.MoveGroup(pickedEmote, spotGroup, Eigen::Vector3f(0, 1, 0));
			break;
			case GLUT_KEY_DOWN:
			myface.MoveGroup(pickedEmote, spotGroup, Eigen::Vector3f(0, -1, 0));
			break;
			case GLUT_KEY_LEFT:
			myface.MoveGroup(pickedEmote, spotGroup, Eigen::Vector3f(-1, 0, 0));
			break;
			case GLUT_KEY_RIGHT:
			myface.MoveGroup(pickedEmote, spotGroup, Eigen::Vector3f(1, 0, 0));
			break;
		}
	}
}

void timerGL(int value)
{
	glutPostRedisplay();
	glutTimerFunc(20, timerGL, 0);
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitWindowSize(400, 400);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutCreateWindow("Andrew Acosta");
	glutMouseFunc(mouseGL);
	glutMotionFunc(mouseMotionGL);
	glutKeyboardFunc(keyboardGL);
	glutSpecialFunc(specialInput);
	glutReshapeFunc(reshapeGL);
	glutDisplayFunc(drawGL);
	glutTimerFunc(20, timerGL, 0);
	loadScene();
	initGL();
	glutMainLoop();
	return 0;
}
