#include "Texture.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

Texture::Texture() :
	filename(""),
	tid(0),
	rv(NULL)
{
	
}

Texture::~Texture()
{
	
}

void Texture::init()
{
	// Load texture
	int w, h, ncomps;
	unsigned char *data = stbi_load(filename.c_str(), &w, &h, &ncomps, 0);

	if(!data) {
		std::cerr << filename << " not found" << std::endl;
	}
	if(ncomps != 3) {
		std::cerr << filename << " must have 3 components (RGB)" << std::endl;
	}
	if((w & (w - 1)) != 0 || (h & (h - 1)) != 0) {
		std::cerr << filename << " must be a power of 2" << std::endl;
	}
	width = w;
	height = h;
	
	// Generate a texture buffer object
	glGenTextures(1, &tid);
	// Bind the current texture to be the newly generated texture object
	glBindTexture(GL_TEXTURE_2D, tid);
	// Load the actual texture data
	// Base level is 0, number of channels is 3, and border is 0.
	glTexImage2D(GL_TEXTURE_2D, 0, ncomps, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	// Generate image pyramid
	glGenerateMipmap(GL_TEXTURE_2D);
	// Set texture wrap modes for the S and T directions
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	// Set filtering mode for magnification and minimification
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	// Unbind
	glBindTexture(GL_TEXTURE_2D, 0);
	// Free image, since the data is now on the GPU
	//stbi_image_free(data);

    if (ncomps == 4) 
    {
        rv = SDL_CreateRGBSurface(0, width, height, 32, rmask, gmask, bmask, amask);
    } 
    else if (ncomps == 3) 
    {
        rv = SDL_CreateRGBSurface(0, width, height, 24, rmask, gmask, bmask, 0);
    } 
    else 
    {
        stbi_image_free(data);
        std::cerr << "ERROR: IMAGE DOESN'T CONTAIN 3/4 components" << std::endl;
        exit(-1);
    }

    memcpy(rv->pixels, data, ncomps * width * height);
    stbi_image_free(data);

}

void Texture::bind(GLint handle, GLint unit)
{
	this->unit = unit;
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_2D, tid);
	glUniform1i(handle, unit);
}

void Texture::unbind()
{
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_2D, 0);
}

Eigen::Vector3f Texture::getPixel(int x, int y) const
{
	Uint32 ret = get_pixel32(rv, x, y);

	Eigen::Vector3f RGB((ret & rmask) >> rshift, (ret & gmask) >> gshift, (ret & bmask) >> bshift);

	// std::cout << "Vector ( " << x << ", " << y << ")"; 
	// std::cout << "RGB Values are: (" << RGB(0) << ", " << RGB(1) << ", " << RGB(2) << ")" << std::endl;  

	return RGB;
}	

Uint32 get_pixel32(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;
        break;

    case 2:
        return *(Uint16 *)p;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        return *(Uint32 *)p;
        break;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}