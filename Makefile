CC=g++
CFLAGS=-ansi -pedantic -Wno-deprecated
INC=-I$(EIGEN3_INCLUDE_DIR)
LIB=-DGL_GLEXT_PROTOTYPES -lglut -lGL -lGLU -lSDL
LIB_OSX=-framework GLUT -framework OpenGL
OBJECT = BlendShape.o Camera.o GLSL.o HUD.o MatrixStack.o Program.o ShapeObj.o Texture.o Vertex.o main.o tiny_obj_loader.o DisplacementMap.o

all: $(OBJECT) 
	clear
	$(CC) $(CFLAGS) $(INC) -g $(OBJECT) $(LIB) -o a1

osx:
	$(CC) $(CFLAGS) $(INC) -g $(OBJECT) $(LIB_OSX) -o a1

%.o: %.cpp
	g++ -g -c $< $(CFLAGS) $(INC) $(LIB)

%.cpp: %.h
	touch $@

run:
	./a1
clean:
	rm -f *~ *.o a1

fullclear:
	clear
	rm -f *~ *.o a1
	$(CC) $(CFLAGS) $(INC) *.cpp *.cc $(LIB) -o a1

cc:
	clear
	rm -f *~ *.o a1
	$(CC) $(CFLAGS) $(INC) *.cpp *.cc $(LIB) -o a1
	./a1