#include <iostream>
#include <Eigen/Dense>
#include "ShapeObj.h"
#include "GLSL.h"

using namespace std;

Eigen::Matrix4f getPlaneEquation(Eigen::Vector3f pt1, Eigen::Vector3f pt2, Eigen::Vector3f pt3);
std::vector< std::vector<unsigned int> > indexLookUp;

ShapeObj::ShapeObj() :
	posBufID(0),
	norBufID(0),
	texBufID(0),
	indBufID(0)
{
}

ShapeObj::~ShapeObj()
{
}

void ShapeObj::load(const string &meshName)
{
	// Load geometry
	// Some obj files contain material information.
	// We'll ignore them for this assignment.
	vector<tinyobj::material_t> objMaterials;
	string err = tinyobj::LoadObj(shapes, objMaterials, meshName.c_str());
	if(!err.empty()) {
		cerr << err << endl;
	}
}

void ShapeObj::init()
{
	// Send the position array to the GPU
	const vector<float> &posBuf = shapes[0].mesh.positions;
	glGenBuffers(1, &posBufID);
	glBindBuffer(GL_ARRAY_BUFFER, posBufID);
	glBufferData(GL_ARRAY_BUFFER, posBuf.size()*sizeof(float), &posBuf[0], GL_STATIC_DRAW);
	
	// Send the normal array (if it exists) to the GPU
	const vector<float> &norBuf = shapes[0].mesh.normals;
	if(!norBuf.empty()) {
		glGenBuffers(1, &norBufID);
		glBindBuffer(GL_ARRAY_BUFFER, norBufID);
		glBufferData(GL_ARRAY_BUFFER, norBuf.size()*sizeof(float), &norBuf[0], GL_STATIC_DRAW);
	} else {
		norBufID = 0;
	}
	
	// Send the texture coordinates array (if it exists) to the GPU
	const vector<float> &texBuf = shapes[0].mesh.texcoords;
	if(!texBuf.empty()) {
		glGenBuffers(1, &texBufID);
		glBindBuffer(GL_ARRAY_BUFFER, texBufID);
		glBufferData(GL_ARRAY_BUFFER, texBuf.size()*sizeof(float), &texBuf[0], GL_STATIC_DRAW);
	} else {
		texBufID = 0;
	}
	
	// Send the index array to the GPU
	const vector<unsigned int> &indBuf = shapes[0].mesh.indices;
	glGenBuffers(1, &indBufID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indBufID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indBuf.size()*sizeof(unsigned int), &indBuf[0], GL_STATIC_DRAW);
	
	// Unbind the arrays
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	assert(glGetError() == GL_NO_ERROR);
}

void ShapeObj::draw(int h_pos, int h_nor, int h_tex) const
{
	// Enable and bind position array for drawing
	GLSL::enableVertexAttribArray(h_pos);
	glBindBuffer(GL_ARRAY_BUFFER, posBufID);
	glVertexAttribPointer(h_pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	// Enable and bind normal array (if it exists) for drawing
	if(norBufID && h_nor >= 0) {
		GLSL::enableVertexAttribArray(h_nor);
		glBindBuffer(GL_ARRAY_BUFFER, norBufID);
		glVertexAttribPointer(h_nor, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}
	
	// Enable and bind texcoord array (if it exists) for drawing
	if(texBufID && h_tex >= 0) {
		GLSL::enableVertexAttribArray(h_tex);
		glBindBuffer(GL_ARRAY_BUFFER, texBufID);
		glVertexAttribPointer(h_tex, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}
	
	// Bind index array for drawing
	int nIndices = (int)shapes[0].mesh.indices.size();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indBufID);
	
	// Draw
	glDrawElements(GL_TRIANGLES, nIndices, GL_UNSIGNED_INT, 0);
	
	// Disable and unbind
	if(texBufID && h_tex >= 0) {
		GLSL::disableVertexAttribArray(h_tex);
	}
	if(norBufID && h_nor >= 0) {
		GLSL::disableVertexAttribArray(h_nor);
	}
	GLSL::disableVertexAttribArray(h_pos);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

/*below is the code for simplifying things, it is from a previous lab*/
void ShapeObj::simplify() {
	tinyobj::mesh_t* mesh = &(shapes[0].mesh);
	int numPoints = mesh->positions.size()/3; 
	int numInds = mesh->indices.size();
	//std::vector<Eigen::Matrix4f> vertexQ;

	//vertexQ.resize(numPoints);
	indexLookUp.resize(numPoints);

	/*for (int i = 0; i < vertexQ.size(); ++i)
	{
		vertexQ[i] << 0, 0, 0, 0,
						0, 0, 0, 0,
						0, 0, 0, 0,
						0, 0, 0, 0;
	}

	Eigen::Vector3f pta, ptb, ptc;
	Eigen::Matrix4f holdMat;*/
	for (int i = 0; i < numInds; i += 3)
	{
		/*pta << Eigen::Vector3f(mesh->positions[mesh->indices[i    ]*3], mesh->positions[mesh->indices[i    ]*3 + 1], mesh->positions[mesh->indices[i    ]*3 +2]);
		ptb << Eigen::Vector3f(mesh->positions[mesh->indices[i + 1]*3], mesh->positions[mesh->indices[i + 1]*3 + 1], mesh->positions[mesh->indices[i + 1]*3 +2]);
		ptc << Eigen::Vector3f(mesh->positions[mesh->indices[i + 2]*3], mesh->positions[mesh->indices[i + 2]*3 + 1], mesh->positions[mesh->indices[i + 2]*3 +2]);

		holdMat = getPlaneEquation(pta, ptb, ptc);*/
		indexLookUp[mesh->indices[i    ]].push_back(mesh->indices[i + 1]);
		indexLookUp[mesh->indices[i    ]].push_back(mesh->indices[i + 2]);
		indexLookUp[mesh->indices[i + 1]].push_back(mesh->indices[i    ]);
		indexLookUp[mesh->indices[i + 1]].push_back(mesh->indices[i + 2]);
		indexLookUp[mesh->indices[i + 2]].push_back(mesh->indices[i    ]);
		indexLookUp[mesh->indices[i + 2]].push_back(mesh->indices[i + 1]);
	}
	
}

void ShapeObj::collapsePts(unsigned int pt1, unsigned int pt2) {
	tinyobj::mesh_t* mesh = &(shapes[0].mesh);
	Eigen::Matrix4f qPt1, qPt2;
	Eigen::Vector3f pta, ptb, ptc;

	qPt1 = Eigen::Matrix4f::Zero();
	qPt2 = Eigen::Matrix4f::Zero();

	pta << Eigen::Vector3f(mesh->positions[pt1*3], mesh->positions[pt1*3+1], mesh->positions[pt1*3+2]);
	for (int i = 0; i < indexLookUp[pt1].size(); i += 2)
	{
		ptb << Eigen::Vector3f(mesh->positions[indexLookUp[pt1][i] * 3], mesh->positions[indexLookUp[pt1][i] * 3+1], mesh->positions[indexLookUp[pt1][i] * 3+2]);
		ptc << Eigen::Vector3f(mesh->positions[indexLookUp[pt1][i+1] * 3], mesh->positions[indexLookUp[pt1][i+1] * 3+1], mesh->positions[indexLookUp[pt1][i+1] * 3+2]);
		
		qPt1 = qPt1 + getPlaneEquation(pta, ptb, ptc);
	}

	//calculate pt2
	pta << Eigen::Vector3f(mesh->positions[pt2*3], mesh->positions[pt2*3+1], mesh->positions[pt2*3+2]);
	for (int i = 0; i < indexLookUp[pt2].size(); i += 2)
	{
		ptb << Eigen::Vector3f(mesh->positions[indexLookUp[pt2][i] * 3], mesh->positions[indexLookUp[pt2][i] * 3+1], mesh->positions[indexLookUp[pt2][i] * 3+2]);
		ptc << Eigen::Vector3f(mesh->positions[indexLookUp[pt2][i+1] * 3], mesh->positions[indexLookUp[pt2][i+1] * 3+1], mesh->positions[indexLookUp[pt2][i+1] * 3+2]);
		
		qPt2 = qPt2 + getPlaneEquation(pta, ptb, ptc);
	}

	Eigen::Matrix4f qHat = qPt1 + qPt2;
	qHat(3) = qHat(7) = qHat(11) = 0;
	qHat(15) = 1;

	Eigen::Vector4f newPt = qHat.inverse()*Eigen::Vector4f(0,0,0,1);

	mesh->positions[pt1*3] = newPt(0);
	mesh->positions[pt1*3 + 1] = newPt(1);
	mesh->positions[pt1*3 + 2]  = newPt(2);
	mesh->positions[pt2*3] = newPt(0);
	mesh->positions[pt2*3 + 1] = newPt(1);
	mesh->positions[pt2*3 + 2] = newPt(2);
}

Eigen::Matrix4f getKPMatrix(Eigen::Vector4f planeEq) {
	Eigen::Matrix4f ret;
	float a = planeEq(0), b = planeEq(1), c = planeEq(2), d = planeEq(3);
	ret << a*a, a*b, a*c, a*d,
			a*b, b*b, b*c, b*d,
			a*c, b*c, c*c, d*c,
			a*d, b*d, c*d, d*d;

	return ret;
}

Eigen::Matrix4f getPlaneEquation(Eigen::Vector3f pt1, Eigen::Vector3f pt2, Eigen::Vector3f pt3) {
	/*pt1 = Eigen::Vector3f(3, -2, 2);
	pt2 = Eigen::Vector3f(1, 0, -3);
	pt3 = Eigen::Vector3f(-2, 1, 1);*/

	Eigen::Vector3f pt21 = pt2 - pt1;
	Eigen::Vector3f pt31 = pt3 - pt1;
	Eigen::Vector3f acrossb = pt21.cross(pt31);

	double d = -(acrossb(0)*pt1(0) + acrossb(1)*pt1(1) + acrossb(2)*pt1(2));

	//cout << "Plane equation: " << acrossb(0) << "x + " << acrossb(1) << "y + " << acrossb(2) << "z + " << d << " = 0" << endl;

	Eigen::Vector4f planeEq = Eigen::Vector4f(acrossb(0), acrossb(1), acrossb(2), d);

	return getKPMatrix(planeEq);
}