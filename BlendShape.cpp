#include <iostream>
#include "BlendShape.h"
#include "GLSL.h"
#include "Program.h"
#include "tiny_obj_loader.h"

using namespace std;

BlendShape::BlendShape() :
	eleBufID(0),
	posBufID(0),
	norBufID(0),
	texBufID(0),
	groupBufID(0)
{
	for (int i = 0; i < 6; ++i) {
		blendNorBufs[i] = blendPosBufs[i] = 0;
	}
}

BlendShape::~BlendShape()
{
}

void BlendShape::loadMesh(const string &meshName)
{
	// Load geometry
	// Some obj files contain material information.
	// We'll ignore them for this assignment.
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> objMaterials;
	string err = tinyobj::LoadObj(shapes, objMaterials, meshName.c_str());
	if(!err.empty()) {
		cerr << err << endl;
	} else {
		posBuf = shapes[0].mesh.positions;
		norBuf = shapes[0].mesh.normals;
		texBuf = shapes[0].mesh.texcoords;
		eleBuf = shapes[0].mesh.indices;

		int numPoints = shapes[0].mesh.positions.size()/3;
		vertices.resize(numPoints);
		for (int i = 0; i < numPoints; ++i) {
			vertices[i] = Vertex();
		}


		Eigen::Vector3f holdPos;
		Eigen::Vector3f holdNorm = Eigen::Vector3f(0,0,0);
		Eigen::Vector2f holdTex = Eigen::Vector2f(0,0);

		numBlends = 0;
		blendPoses.reserve(8);
		blendNorms.reserve(8);
	}
}

void BlendShape::addBlend(const string &meshName)
{
	// Load geometry
	// Some obj files contain material information.
	// We'll ignore them for this assignment.
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> objMaterials;
	string err = tinyobj::LoadObj(shapes, objMaterials, meshName.c_str());
	if(!err.empty()) {
		cerr << err << endl;
	} else {
		/*might comment out the next line too*/
		int numPoints = shapes[0].mesh.positions.size()/3;
		if (shapes[0].mesh.positions.size() != posBuf.size()) {
			cout << "ERROR! This blend has a different number of vertices" << endl;
			cout << "ERROR! This blend has " << shapes[0].mesh.positions.size() << " vertices while the base has " << posBuf.size() << endl;
		} else {
			std::vector<float> posOffset;
			std::vector<float> normOffset;

			posOffset.reserve(posBuf.size());
			normOffset.reserve(norBuf.size());

			for (int i = 0; i < posBuf.size(); ++i) {
				posOffset.push_back(shapes[0].mesh.positions[i] - posBuf[i]);
			}

			for (int j = 0; j < norBuf.size(); ++j) {
				normOffset.push_back(shapes[0].mesh.normals[j] - norBuf[j]);
			}

			blendPoses.push_back(posOffset);
			blendNorms.push_back(normOffset);

			initBlend(numBlends);

			numBlends++;
		}
	}
}

void BlendShape::init()
{
	Segment(.75);
	if (groupBuf.empty()) {
		groupBufID = 0;
	} else {
		// Send the group array to the GPU
		glGenBuffers(1, &groupBufID);
		glBindBuffer(GL_ARRAY_BUFFER, groupBufID);
		glBufferData(GL_ARRAY_BUFFER, groupBuf.size()*sizeof(float), &groupBuf[0], GL_STATIC_DRAW);
	}

	// Send the position array to the GPU
	glGenBuffers(1, &posBufID);
	glBindBuffer(GL_ARRAY_BUFFER, posBufID);
	glBufferData(GL_ARRAY_BUFFER, posBuf.size()*sizeof(float), &posBuf[0], GL_STATIC_DRAW);
	
	// Send the normal array to the GPU
	if(norBuf.empty()) {
		norBufID = 0;
	} else {
		glGenBuffers(1, &norBufID);
		glBindBuffer(GL_ARRAY_BUFFER, norBufID);
		glBufferData(GL_ARRAY_BUFFER, norBuf.size()*sizeof(float), &norBuf[0], GL_STATIC_DRAW);
	}
	
	// Send the texture array to the GPU
	if(texBuf.empty()) {
		texBufID = 0;
	} else {
		glGenBuffers(1, &texBufID);
		glBindBuffer(GL_ARRAY_BUFFER, texBufID);
		glBufferData(GL_ARRAY_BUFFER, texBuf.size()*sizeof(float), &texBuf[0], GL_STATIC_DRAW);
	}
	
	// Send the element array to the GPU
	glGenBuffers(1, &eleBufID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eleBufID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, eleBuf.size()*sizeof(unsigned int), &eleBuf[0], GL_STATIC_DRAW);
	
	// Unbind the arrays
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	assert(glGetError() == GL_NO_ERROR);
}

void BlendShape::initBlend(int blendNum) {
	// Send the position array to the GPU
	glGenBuffers(1, &blendPosBufs[blendNum]);
	glBindBuffer(GL_ARRAY_BUFFER, blendPosBufs[blendNum]);
	glBufferData(GL_ARRAY_BUFFER, blendPoses[blendNum].size()*sizeof(float), &blendPoses[blendNum][0], GL_STATIC_DRAW);

	// Send the normal array to the GPU
	if(blendNorms[blendNum].empty()) {
		blendNorBufs[blendNum] = 0;
	} else {
		glGenBuffers(1, &blendNorBufs[blendNum]);
		glBindBuffer(GL_ARRAY_BUFFER, blendNorBufs[blendNum]);
		glBufferData(GL_ARRAY_BUFFER, blendNorms[blendNum].size()*sizeof(float), &blendNorms[blendNum][0], GL_STATIC_DRAW);
	}

	// Unbind the arrays
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	assert(glGetError() == GL_NO_ERROR);
}

void BlendShape::SetWeights(float weights[], int length) {

}

void BlendShape::drawBlends(const Program *prog) const {

	// Bind position buffer
	int h_pos[6] = {prog->getAttribute("blendPose1"), prog->getAttribute("blendPose2"), 
					prog->getAttribute("blendPose3"), prog->getAttribute("blendPose4"), 
					prog->getAttribute("blendPose5"), prog->getAttribute("blendPose6")};

	for (int i = 0; i < 6; ++i) {
		GLSL::enableVertexAttribArray(h_pos[i]);
		glBindBuffer(GL_ARRAY_BUFFER, blendPosBufs[i]);
		glVertexAttribPointer(h_pos[i], 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);
	}

	// Bind normal buffer
	int h_nor[6] = {prog->getAttribute("blendNorm1"), prog->getAttribute("blendNorm2"), 
					prog->getAttribute("blendNorm3"), prog->getAttribute("blendNorm4"), 
					prog->getAttribute("blendNorm5"), prog->getAttribute("blendNorm6")};
	for (int i = 0; i < 6; ++i) {
		if(h_nor[i] != -1 && blendNorBufs[i] != 0) {
			GLSL::enableVertexAttribArray(h_nor[i]);
			glBindBuffer(GL_ARRAY_BUFFER, blendNorBufs[i]);
			glVertexAttribPointer(h_nor[i], 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);
		}
	}
}

void BlendShape::draw(const Program *prog) const
{
	assert(prog);

	//Bind group buffer
	int h_group = prog->getAttribute("group");
	GLSL::enableVertexAttribArray(h_group);
	glBindBuffer(GL_ARRAY_BUFFER, groupBufID);
	glVertexAttribPointer(h_group, 1, GL_FLOAT, GL_FALSE, 0, (const void *)0);
	
	// Bind position buffer
	int h_pos = prog->getAttribute("vertPos");
	GLSL::enableVertexAttribArray(h_pos);
	glBindBuffer(GL_ARRAY_BUFFER, posBufID);
	glVertexAttribPointer(h_pos, 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);
	
	// Bind normal buffer
	int h_nor = prog->getAttribute("vertNor");
	if(h_nor != -1 && norBufID != 0) {
		GLSL::enableVertexAttribArray(h_nor);
		glBindBuffer(GL_ARRAY_BUFFER, norBufID);
		glVertexAttribPointer(h_nor, 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);
	}
	
	// Bind texcoords buffer
	int h_tex = -1; //prog->getAttribute("vertTex");
	if(h_tex != -1 && texBufID != 0) {
		GLSL::enableVertexAttribArray(h_tex);
		glBindBuffer(GL_ARRAY_BUFFER, texBufID);
		glVertexAttribPointer(h_tex, 2, GL_FLOAT, GL_FALSE, 0, (const void *)0);
	}
	
	// Bind element buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eleBufID);

	//Bind Blend arrays
	drawBlends(prog);

	// Draw
	glDrawElements(GL_TRIANGLES, (int)eleBuf.size(), GL_UNSIGNED_INT, (const void *)0);
	
	// Disable and unbind
	if(h_tex != -1) {
		GLSL::disableVertexAttribArray(h_tex);
	}
	if(h_nor != -1) {
		GLSL::disableVertexAttribArray(h_nor);
	}
	GLSL::disableVertexAttribArray(h_pos);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void BlendShape::calculateDefMap(vector <DisplaceMapVert>& ret) {
	int numVerts = posBuf.size()/3;
	ret.resize(numVerts);

	DisplaceMapVert testVal;

	for (int vert = 0; vert < numVerts; ++vert) {
		for (int exp = 0; exp < numBlends; ++exp) {

			testVal = DisplaceMapVert();
			testVal.disp = Eigen::Vector3f(blendPoses[exp][vert*3], blendPoses[exp][vert*3 + 1], blendPoses[exp][vert*3 + 2]);
			testVal.vertNum = vert;
			testVal.dist = abs(testVal.disp.norm());

			if (exp == 0) {
				ret[vert] = testVal;
			} else {
				if (testVal.dist > ret[vert].dist) {
					ret[vert] = testVal;
				}
			}
		}
	}
}

void BlendShape::Segment(float t) {
	vector< vector <int> > groups;

	groups.resize(5);

	calculateDefMap(disp);
	std::vector <DisplaceMapVert> sorted = mergeSort(disp);
	int v1, v2, v3;

	for (int i = 0; i < eleBuf.size(); i += 3) {
		v1 = eleBuf[i];
		v2 = eleBuf[i + 1];
		v3 = eleBuf[i + 2];

		vertices[v1].AddConnection(v2);
		vertices[v1].AddConnection(v3);

		vertices[v2].AddConnection(v1);
		vertices[v2].AddConnection(v3);
 
		vertices[v3].AddConnection(v1);
		vertices[v3].AddConnection(v2);
	}

	int onGroup = 0;

	for (int i = 0; i < sorted.size() && onGroup < 5; ++i) {
		if (!vertices[sorted[i].vertNum].used) {
			groups[onGroup] = makeGroup(sorted[i].vertNum, t*sorted[i].dist);
			++onGroup;
		}
	}

	for (int i = 0; i < onGroup; ++i) {
		cout << "group " << i << " has " << groups[i].size() << " points" << endl;
	}

	groupBuf.clear();
	groupBuf.resize(posBuf.size());

	for (int i = 0; i < groupBuf.size(); ++i) {
		groupBuf[i] = 0;
	}

	for (int g = 0; g < onGroup; ++g) {
		//cout << endl << "Group " << g << " has:" << endl;
		for (int i = 0; i < groups[g].size(); ++i) {
			groupBuf[ groups[g][i] ] = g + 1;
			//cout << groupBuf[ groups[g][i] ] << ", ";
		}
		//cout << endl;
	}

	//To confirm that there are groups in groupBuf
	/*for (int i = 0; i < groupBuf.size(); ++i) {
		if (groupBuf[i] != 0) {
			cout << "at " << i << ": " << groupBuf[i] << endl;
		}
	}*/
}

std::vector <int> BlendShape::makeGroup(int vert, float t) {
	std::vector<int> group;

	if (!vertices[vert].used) {
		group.push_back(vert);
		vertices[vert].used = true;
		makeGroupHelper(group, vert, t);
	}

	return group;
}

void BlendShape::makeGroupHelper(vector <int>& group, int vert, float t) {
	int numCon = vertices[vert].connections.size();
	int check;

	for (int i = 0; i < numCon; ++i)
	{
		check = vertices[vert].connections[i];
		if (disp[check].vertNum != check) {
			cout << "ERROR!" << endl;
		}

		
		if (!vertices[check].used && disp[check].dist > t) {
			vertices[check].used = true;
			group.push_back(check);
			makeGroupHelper(group, check, t);
		}
	}
}

void MoveGroup(int group, Eigen::Vector3f dir) {
	
}