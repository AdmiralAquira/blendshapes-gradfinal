#include "Vertex.h"

using namespace std;

Vertex::Vertex() {
	used = false;
	connections.reserve(8);
}

void Vertex::AddConnection (int con) {
	for (int i = 0; i < connections.size(); ++i) {
		if (connections[i] == con) {
			return;
		}
	}

	connections.push_back(con);
}