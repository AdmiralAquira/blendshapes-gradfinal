#version 130

struct Material {
  vec3 aColor;
  vec3 dColor;
  vec3 sColor;
  float shine;
};

attribute vec3 vertPos;
attribute vec3 vertNor;
attribute vec2 vertTex;

attribute vec3 blendPose1;
attribute vec3 blendPose2;
attribute vec3 blendPose3;
attribute vec3 blendPose4;
attribute vec3 blendPose5;
attribute vec3 blendPose6;

attribute vec3 blendNorm1;
attribute vec3 blendNorm2;
attribute vec3 blendNorm3;
attribute vec3 blendNorm4;
attribute vec3 blendNorm5;
attribute vec3 blendNorm6;

attribute float group;

uniform mat4 P;		//projection matrix
uniform mat4 MV;	//model-view matrix
uniform Material uMat;
uniform float isDot;

uniform float weights[6];
uniform float spotGroup;

varying vec3 fragNor;
varying vec4 light;
varying float dist;
varying vec3 fragColor;


void main()
{
	vec4 lightPos = vec4(0,1,0,1);
	/*set up lighting before camera and after translations*/
	vec4 newPos = vec4(vertPos, 1.0);
	light = lightPos - newPos; //the vector of light position - v position
	light = normalize(light);

	/*calculate reflection*/
	//Refl = vec3(-light + 2.0*(clamp(dot(tNormal, light), 0.0, 1.0))*tNormal);
	  
	/*calculate the distance*/
	dist = distance(newPos, lightPos);
	dist = 1.0;//(1.0/(dist));

	newPos = newPos + vec4(blendPose1*weights[0], 0.0);
	newPos = newPos + vec4(blendPose2*weights[1], 0.0);
	newPos = newPos + vec4(blendPose3*weights[2], 0.0);
	newPos = newPos + vec4(blendPose4*weights[3], 0.0);
	newPos = newPos + vec4(blendPose5*weights[4], 0.0);
	newPos = newPos + vec4(blendPose6*weights[5], 0.0);

	vec3 newNorm = vertNor + blendNorm1*weights[0];
	newNorm = newNorm + blendNorm2*weights[1];
	newNorm = newNorm + blendNorm3*weights[2];
	newNorm = newNorm + blendNorm4*weights[3];
	newNorm = newNorm + blendNorm5*weights[4];
	newNorm = newNorm + blendNorm6*weights[5];

	gl_Position = P * MV * newPos;
	fragNor = (MV * vec4(newNorm, 0.0)).xyz;

	fragColor = vec3(0, 0, 0);
	if (group == spotGroup) {
		if (group > 4.9) {
			fragColor = vec3(1, 0, 0);
		} else if (group > 3.9) {
			fragColor = vec3(0, 1, 0);
		} else if (group > 2.9) {
			fragColor = vec3(0, 0, 1);
		} else if (group > 1.9) {
			fragColor = vec3(1, 0, 1);
		} else if (group > .9) {
			fragColor = vec3(0, 1, 1);
		}
	}

	if (spotGroup == 6) {
		//for when we show groups
		if (group > 4.9) {
			fragColor = vec3(1, 0, 0);
		} else if (group > 3.9) {
			fragColor = vec3(0, 1, 0);
		} else if (group > 2.9) {
			fragColor = vec3(0, 0, 1);
		} else if (group > 1.9) {
			fragColor = vec3(1, 0, 1);
		} else if (group > .9) {
			fragColor = vec3(0, 1, 1);
		}
	}
}
