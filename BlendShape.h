#pragma once
#ifndef _BLENDSHAPE_H_
#define _BLENDSHAPE_H_

#include <vector>
#include "Texture.h"
#include "Vertex.h"
#include "DisplacementMap.h"

class Program;

class BlendShape
{
public:
	BlendShape();
	virtual ~BlendShape();

	void loadMesh(const std::string &meshName);
	void addBlend(const std::string &meshName);
	void init();
	void initBlend();
	void SetWeights(float weights[], int length);
	void draw(const Program *p) const;	
	void drawBlends(const Program *p) const;
	void Segment(float t);
	void MoveGroup(int blend, int group, Eigen::Vector3f dir);
	
private:
	void initBlend(int blendNum);
	void calculateDefMap(std::vector <DisplaceMapVert>& ret);
	std::vector <int> makeGroup(int vert, float t);
	void makeGroupHelper(std::vector <int>& group, int vert, float t) ;

	int numBlends;
	std::vector<Vertex> vertices;
	std::string texture;

	std::vector< std::vector<float> > blendPoses;
	std::vector< std::vector<float> > blendNorms;

	std::vector<unsigned int> eleBuf;
	std::vector<float> posBuf;
	std::vector<float> norBuf;
	std::vector<float> texBuf;
	unsigned eleBufID;
	unsigned posBufID;
	unsigned norBufID;
	unsigned texBufID;

	unsigned blendPosBufs[6];
	unsigned blendNorBufs[6];

	unsigned groupBufID;
	std::vector<float> groupBuf;

	std::vector <DisplaceMapVert> disp; //displacement map
	
};

#endif
