#include "DisplacementMap.h"

using namespace std;

vector<DisplaceMapVert> merge(vector<DisplaceMapVert> left, vector<DisplaceMapVert> right)
{
    vector<DisplaceMapVert> result;
    result.reserve(left.size() + right.size());
    
    while (left.size() > 0 || right.size() > 0) {
        if (left.size() > 0 && right.size() > 0) {
            if (left.front().dist /*<=*/ >= right.front().dist) {
                result.push_back(left.front());
                left.erase(left.begin());
            } 
            else {
                result.push_back(right.front());
                right.erase(right.begin());
            }
        }  else if (left.size() > 0) {
            for (int i = 0; i < left.size(); i++)
                result.push_back(left[i]);
            break;
        }  else if (right.size() > 0) {
            for (int i = 0; i < right.size(); i++)
                result.push_back(right[i]);
            break;
        }
    }
    return result;
}

vector<DisplaceMapVert> mergeSort(vector<DisplaceMapVert> m)
{
    if (m.size() <= 1)
        return m;

    vector<DisplaceMapVert> left, right, result;
    int middle = (m.size()+ 1) / 2;

    left.reserve(middle);
    right.reserve(middle);
    for (int i = 0; i < middle; i++) {
        left.push_back(m[i]);
    }

    for (int i = middle; i < m.size(); i++) {
        right.push_back(m[i]);
    }

    left = mergeSort(left);
    right = mergeSort(right);
    result = merge(left, right);

    return result;
}